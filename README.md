chgtw
======

概要
---------

Cloud9 IDEでTouchPad(のエミュレータ)用の壁紙チェンジャーを作ってみる実験のリポジトリ。

詳細は「[Cloud9 IDEで作るTouchPad用壁紙チェンジャー その１](http://zawakei.blog23.fc2.com/blog-entry-356.html)」と「[Cloud9 IDEで作るTouchPad用壁紙チェンジャー その２](http://zawakei.blog23.fc2.com/blog-entry-357.html)」」


インストール
---------------
* [underscore](https://github.com/documentcloud/underscore)と[gmモジュール](https://github.com/aheckmann/gm)が必要です。あらかじめTouchPadエミュレータ上にインストールしておいてください(エミュレータへのインストールは「[Cloud9 IDEで作るTouchPad用壁紙チェンジャー その２](http://zawakei.blog23.fc2.com/blog-entry-357.html)」」を参照)。

* chgtwk自体のインストールは`git clone https://bitbucket.org/hankei6km/chgtw`でPC上へリポジトリを複製した後に、TouchPadエミュレータの適当なディレクトリにコピーしてください。


利用方法
----------
* `node main.js`を実行すると、5分間隔でTouchPadエミュレータ内の`/media/internal/wallpapers/`からランダムに画像ファイルを選択し、壁紙にします。

* `node main_tile.js`を実行すると、5分間隔でTouchPadエミュレータ内の`/media/internal/wallpapers/`に保存されている画像ファイルを連結(タイル状に)し、右下に日付を書き込んだ後、壁紙にします。
