#!/bin/node
//----
// Copyright (c) 2011 hankei6km
// License: MIT License (http://opensource.org/licenses/mit-license.php)
//----
/**
 * @fileOverview TouchPad用壁紙チェンジャー(タイル版)メイン処理。
 * @author hankei6km
 */
var fs = require("fs");
var path = require("path");
var getimages = require("getimages");
var tileimg = require('tileimg');
var overlapinfo = require('overlapinfo');
var wallpaper = require("wallpaper");

var TMP_PATH = path.join(__dirname, ".temp");

var WALLPAPER_WIDTH = 1024;
var WALLPAPER_HEIGHT = 768;
var TILE_HORIZONTAL_NUM = 3;
var TILE_VERTICAL_NUM = 3;
var TILE_WIDTH = Math.floor(WALLPAPER_WIDTH / TILE_HORIZONTAL_NUM); //モジュール内でもサイズを計算してるので、ここだけ変更してタイルのサイズは変わらない
var TILE_HEIGHT = Math.floor(WALLPAPER_HEIGHT / TILE_VERTICAL_NUM);

var WALLPAPER_PATH = "/media/internal/wallpapers";
var INTERVAL_SEC = 60 * 5;

var cnt = 0;

/**
 * タイル状壁紙の作成インポート設定の定期実行関数。
 */
var chgFunc = function() {
        getimages({
            path: WALLPAPER_PATH,
            onComplete: function(err, resp) {
                if (!err) {
                    var len = resp.files.length;
                    for (var idx = 1; idx < len; idx++) {
                        var ridx = Math.floor(Math.random() * idx);
                        var tmp = resp.files[idx];
                        resp.files[idx] = resp.files[ridx];
                        resp.files[ridx] = tmp;
                    }
                    tileimg({
                        imgWidth: WALLPAPER_WIDTH,
                        imgHeight: WALLPAPER_HEIGHT,
                        imgFileType: ".png",
                        horizontalNum: TILE_HORIZONTAL_NUM,
                        verticalNum: TILE_VERTICAL_NUM,
                        files: resp.files,
                        tmpPath: TMP_PATH,
                        onComplete: function(err, resp) {
                            if (!err) {
                                console.log("----overlap info");
                                overlapinfo({
                                    baseImgFile: resp.tileImgFile,
                                    width: TILE_WIDTH,
                                    height: TILE_HEIGHT,
                                    x: TILE_WIDTH * 2,
                                    y: TILE_HEIGHT * 2,
                                    tmpPath: TMP_PATH,
                                    onComplete: function(err) {
                                        if (!err) {
                                            wallpaper({
                                                parameters: {
                                                    target: resp.tileImgFile,
                                                    "focusX": 1.0,
                                                    "focusY": 1.0,
                                                    "scale": 0.5
                                                },
                                                onComplete: function(err, response) {
                                                    if (!err) {
                                                        console.log("----done:" + (cnt++));
                                                        setTimeout(chgFunc, INTERVAL_SEC * 1000); //次回の更新をセットする
                                                    }
                                                    else {
                                                        console.error("change wallpaper failed");
                                                        console.dir(e);
                                                    }
                                                }
                                            });
                                        }
                                        else {
                                            console.error("overlap info fail:" + err);
                                        }
                                    }
                                });
                            }
                            else {
                                console.error("gen tile fail:" + err);
                            }
                        }
                    });
                }
                else {
                    console.error("getWallpaper fail");
                    console.dir(err);
                }
            }
        });
    };
    
fs.mkdir(TMP_PATH, 0755, function(err) {
    chgFunc();
});