#!/bin/node
//----
// Copyright (c) 2011 hankei6km
// License: MIT License (http://opensource.org/licenses/mit-license.php)
//----
/**
 * @fileOverview TouchPad用壁紙チェンジャーメイン処理。
 * @author hankei6km
 */
var wallpaper = require("wallpaper");
var getimages = require("getimages");

var WALLPAPER_PATH = "/media/internal/wallpapers";
var INTERVAL_SEC = 60*5;

var cnt=0;
var chgFunc = function() {
    getimages({ //壁紙にするファイルの一覧を取得する
        path: WALLPAPER_PATH,
        onComplete: function(err, resp) {
            var idx = Math.floor(Math.random() * resp.files.length); //壁紙にするファイルをランダムに選択する
            wallpaper({ //壁紙を変更する
                parameters: {
                    target: resp.files[idx]
                },
                onComplete: function(err) {
                    //壁紙変更が完了した
                    if (!err) {
                        console.log("changed:"+(cnt++));
                    }
                    else {
                        console.log("failed");
                        console.dir(err);
                    }
                    setTimeout(chgFunc, INTERVAL_SEC * 1000); //次回の更新をセットする
                }
            });
        }
    });
};
chgFunc();