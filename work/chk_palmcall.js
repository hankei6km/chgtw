var PalmBus = require("palmbus");

//service: "palm://com.palm.systemservice/time/",method: "getSystemTime"を示すuri
var uri = "palm://com.palm.systemservice/time/getSystemTime";

var handle = new PalmBus.Handle("", false);
var requester = handle.call;

var p = requester.call(handle, uri, JSON.stringify({}));
p.addListener('response', function(resp) { //レスポンスを受け取るハンドラー
    var payload = {};
    try{
        payload = JSON.parse(resp.payload()); //結果を取り出す
    }catch(e){
    }
    if (payload.returnValue !== false) { //とりあえずえreturnValueでエラーか判定
        console.log("service api call success");
    }
    else {
        console.log("service api failed success");
    }
    console.dir(payload);
});