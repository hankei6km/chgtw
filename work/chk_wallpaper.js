var PalmCall = require("palmcall");
PalmCall({
    service: "palm://com.palm.systemservice/wallpaper/",
    method: "importWallpaper",
    parameters: {
        target: "/media/internal/wallpapers/01.jpg"
    },
    onComplete: function(err, response) {
        if (!err) {
            console.error("importWallpaper complete");
            console.dir(response);
            PalmCall({
                service: "palm://com.palm.systemservice/",
                method: "setPreferences",
                parameters: {
                    wallpaper: response.wallpaper
                },
                onComplete: function(err, response) {
                    if (!err) {
                        console.log("setPreferences wallpaper complete");
                        console.dir(response);
                    }
                    else {
                        console.error("setPreferences wallpaper failed");
                        console.dir(err);
                    }
                }
            });
        }
        else {
            console.error("importWallpaper failed");
            console.dir(err);
        }
    }
});