//----
// Copyright (c) 2011 hankei6km
// License: MIT License (http://opensource.org/licenses/mit-license.php)
//----
/**
 * @fileOverview qunit-tapを利用したpalmcallモジュールのテスト<br>
 * ≪注意≫テストが完了してもプロセスが終了しないので、手動で停止する必要がある。
 * @author hankei6km
 */

require("../test_helper.js");

var PalmCall = require('palmcall');

/**
 * getSystemTimeがonSuccessになるか確認する
 */
QUnit.asyncTest("getSystemTime_normal", 1, function() {

    PalmCall({
        service: "palm://com.palm.systemservice/time/",
        method: "getSystemTime",
        parameters: {},
        onComplete: function(err,response) {
            if(!err){
                assert.equal(typeof(response.utc),"number", "done");
            }
            QUnit.start();
        }.bind(this)
    });

});

/**
 * 存在しないメソッドでがonFailureになるか確認する
 */
QUnit.asyncTest("unknown_method", 1, function() {

    PalmCall({
        service: "palm://com.palm.systemservice/time/",
        method: "getSystemTime111",
        parameters: {},
        onComplete: function(err,response) {
            if(err){
                assert.ok(true, "done");
            }
            QUnit.start();
        }.bind(this)
    
    });

});

QUnit.start();