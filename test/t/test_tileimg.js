//----
// Copyright (c) 2011 hankei6km
// License: MIT License (http://opensource.org/licenses/mit-license.php)
//----
/**
 * @fileOverview qunit-tapを利用したgenthumbsモジュールのテスト<br>
 * テストデータとして<br>
 * ・元になる画像ファイル:images/chk??.png(??は01～04)<br>
 * ・比較用のサムネイル:images/cmp/tile/tile.png<br>
 * 一時作業ディレクトリとしてimages/tile/
 * がそれぞれ必要となる。
 * また、作成されたサムネイルの比較用に外部コマンド(デフォルトではcmp)使っているので、<br>
 * これも用意する必要がある。
 * @author hankei6km
 */
require("../test_helper.js");

var path = require('path');
var spawn = require('child_process').spawn;

var TileImg = require('tileimg');

var cmp = "cmp"; //バイナリファイルの比較に使う外部コマンド(同じとき:0 異なるとき:1 を返すことを期待している)
var filesNum = 4;
var getFiles = function(num, dirName, suffix) {
        var ret = new Array(num);
        for (var idx = 1; idx < num + 1; idx++) {
            ret[idx - 1] = path.join(dirName, "chk" + (idx < 10 ? "0" + idx : "" + idx) + suffix);
        }
        return ret;
    };

/**
 * タイルイメージを作成して基準となる画像と比較
 */
QUnit.asyncTest("gentile", 3, function() {
    var files = getFiles(filesNum, path.join(__dirname, "images"), ".png");
    var tileImgFile=path.join(__dirname, "images", "tile", "tile.png");

    TileImg({
        imgFileType: ".png",
        files: files,
        imgWidth: 200,
        imgHeight: 200,
        horizontalNum: 2,
        verticalNum: 2,
        tmpPath: path.join(__dirname, "images", "tile"),
        onComplete: function(err,resp) {
            //ステータスのチェック
            assert.equal(err, null, "complete status");
            assert.equal(resp.tileImgFile, tileImgFile, "resp chk");
            //タイルイメージと基準になるファイルを比較
            var s = spawn(cmp, [tileImgFile, path.join(__dirname, "images", "cmp", "tile", "tile.png")]);
            s.on('exit', function(code) {
                assert.equal(code, 0, "image compare");
                QUnit.start();
            });
        }
    });

});

/**
 * エラーになったときに、onCompleteにエラーが渡されるかチェック
 */
QUnit.asyncTest("invalid_file_path", 2, function() {
    var files = getFiles(filesNum, path.join(__dirname, "images"), ".png");

    TileImg({
        imgFileType: ".png",
        files: files,
        imgWidth: 200,
        imgHeight: 200,
        horizontalNum: 2,
        verticalNum: 2,
        tmpPath: path.join(__dirname, "images", "tile_dummy"), //存在しないディレクトリを渡す
        onComplete: function(err) {
            assert.notEqual(err, null, "complete status");
            assert.notEqual(String(err).match("tile_dummy"), null, "msg chk");
            QUnit.start();
        }
    });

});


QUnit.start();