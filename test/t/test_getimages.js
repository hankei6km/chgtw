//----
// Copyright (c) 2011 hankei6km
// License: MIT License (http://opensource.org/licenses/mit-license.php)
//----
/**
 * @fileOverview qunit-tapを利用したgetimagesモジュールのテスト<br>
 * @author hankei6km
 */
require("../test_helper.js");
var path = require('path');

var getimages = require('getimages');

/**
 * imagesディレクトリの一覧を作成し、ディレクトリなどを除いたファイル一覧になっているかチェック。
 */
QUnit.asyncTest("getimages", 3, function() {
    getimages({
        path:path.join(__dirname, "images"),
        onComplete: function(err,resp) {
            var len=resp.files.length;
            //ステータスのチェック
            assert.equal(err, null, "complete status");
            assert.equal(len, 5, "files array length");
            //一覧のチェック
            resp.files.sort();
            for(var idx=0;idx<len;idx++){
                resp.files[idx]=path.basename(resp.files[idx]);
            }
            assert.deepEqual(resp.files, ["chk01.png","chk02.png","chk03.png","chk04.png","chk10.png"], "files array values");
            QUnit.start();
        }
    });

});


QUnit.start();