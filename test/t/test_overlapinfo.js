//----
// Copyright (c) 2011 hankei6km
// License: MIT License (http://opensource.org/licenses/mit-license.php)
//----
/**
 * @fileOverview qunit-tapを利用したoverlapinfoモジュールのテスト<br>
 * テストデータとして<br>
 * ・元になる画像ファイル:images/chk1o.png
 * ・比較用のサムネイル:images/cmp/overlap/overlap.png<br>
 * 一時作業ディレクトリとしてimages/overlap/
 * がそれぞれ必要となる。
 * また、作成されたサムネイルの比較用に外部コマンド(デフォルトではcmp)使っているので、<br>
 * これも用意する必要がある。
 * @author hankei6km
 */
require("../test_helper.js");

var path = require('path');
var spawn = require('child_process').spawn;


var OverlapInfo = require("overlapinfo");

var cmp = "cmp"; //バイナリファイルの比較に使う外部コマンド(同じとき:0 異なるとき:1 を返すことを期待している)

/**
 * タイルイメージを作成して基準となる画像と比較
 */
QUnit.asyncTest("overlapinfo", 2, function() {
    var overlapImgFile=path.join(__dirname, "images", "overlap","overlap.png");

    OverlapInfo({
        baseImgFile: path.join(__dirname, "images", "chk10.png"),
        width:512,
        height:256,
        x:512,
        y:512,
        outImgFile:overlapImgFile,
        parameters:{
            date:new Date("2011/12/22") //比較用に時刻を固定する
        },
        onComplete:function(err){
            assert.equal(err, null, "complete status");
            //出力された画像と基準になるファイルを比較
            var s = spawn(cmp, [overlapImgFile, path.join(__dirname, "images", "cmp", "overlap", "overlap.png")]);
            s.on('exit', function(code) {
                assert.equal(code, 0, "image compare");
                QUnit.start();
            });
        }
    });

});

QUnit.start();