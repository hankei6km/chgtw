//----
// Copyright (c) 2011 hankei6km
// License: MIT License (http://opensource.org/licenses/mit-license.php)
//----
/**
 * @fileOverview qunit-tapを利用したgenthumbsモジュールのテスト<br>
 * テストデータとして<br>
 * ・元になる画像ファイル:images/chk??.png(??は01～04)<br>
 * ・比較用のサムネイル:images/cmp/thumbs/chk??.png(??は01～04)<br>
 * 一時作業ディレクトリとしてimages/thumbs/
 * がそれぞれ必要となる。
 * また、作成されたサムネイルの比較用に外部コマンド(デフォルトではcmp)使っているので、<br>
 * これも用意する必要がある。
 * @author hankei6km
 */

require("../test_helper.js");

var path = require('path');
var spawn = require('child_process').spawn;
var ReqQue = require("reqque");

var genThumbs = require('genthumbs');

var cmp = "cmp"; //バイナリファイルの比較に使う外部コマンド(同じとき:0 異なるとき:1 を返すことを期待している)
var filesNum = 4;
var getFiles = function(num, dirName, suffix) {
        var ret = new Array(num);
        for (var idx = 1; idx < num + 1; idx++) {
            ret[idx - 1] = path.join(dirName, "chk" + (idx < 10 ? "0" + idx : "" + idx) + suffix);
        }
        return ret;
    };
/**
 * サムネイルを作成して基準となる画像と比較
 */
QUnit.asyncTest("genthumbs", 2 + filesNum, function() {
    var files = getFiles(filesNum, path.join(__dirname, "images"), ".png");
    var chkThumbFiles = getFiles(filesNum, path.join(__dirname, "images", "thumbs"), ".png");
    var cmpFiles = getFiles(filesNum, path.join(__dirname, "images", "cmp", "thumbs"), ".png");

    genThumbs({
        thumbFileType: ".png",
        files: files,
        thumbWidth: 200,
        thumbHeight: 150,
        thumbOut: path.join(__dirname, "images", "thumbs"),
        onComplete: function(err, resp) {
            //サムネイル作成できた時点でのチェック
            assert.equal(err, null, "complete status");
            assert.deepEqual(resp.thumbFiles, chkThumbFiles, "thumb files");

            //サムネイルと基準になるファイルを比較
            var cmpQue = ReqQue({
                onComplete: function() { //各比較の終了を同期させる
                    QUnit.start();
                }
            });
            for (var idx = 0; idx < filesNum; idx++) {
                cmpQue.push({
                    run: function(idx, qh) {
                        var s = spawn(cmp, [resp.thumbFiles[idx], cmpFiles[idx]]);
                        s.on('exit', function(code) {
                            assert.equal(code, 0, "cmp:" + idx);
                            qh.done();
                        });
                    }.bind(this, idx)
                });
            }
        }.bind(this)
    });

});

/**
 * エラーになったときに、onCompleteにエラーが渡されるかチェック
 */
QUnit.asyncTest("invalid_file_path", 2, function() {
    var files = getFiles(filesNum, path.join(__dirname, "images"), ".png");

    genThumbs({
        thumbFileType: ".png",
        files: files,
        thumbWidth: "abcdef", //正しくない値を渡す
        thumbHeight: 150,
        thumbOut: path.join(__dirname, "images", "thumbs"),
        onComplete: function(err, resp) {
            assert.notEqual(err, null, "complete status");
            assert.notEqual(String(err).match("malformed"), null, "msg chk");
            QUnit.start();
        }.bind(this)
    });

});

QUnit.start();