//----
// Copyright (c) 2011 hankei6km
// License: MIT License (http://opensource.org/licenses/mit-license.php)
//----
/**
 * @fileOverview qunit-tapを利用したreqqueモジュールのテスト
 * @author hankei6km
 */

require("../test_helper.js");
var ReqQue = require("reqque");

/**
 * リクエストが１つで、runメソッドが非同期になってない場合
 */
QUnit.asyncTest("single_req", function() {
    var runFlag = false;

    var rq = ReqQue({
        onComplete: function() {
            assert.ok(runFlag, "done");
            QUnit.start();
        }
    });
    rq.push({
        run: function(queHandler) {
            runFlag = true;
            queHandler.done();
        }
    });
});

/**
 * リクエストが複数(同時実行数を越えない)で、runメソッドが非同期になってない場合
 */
QUnit.asyncTest("multi_req_serial", function() {
    var baseFlags = [true, true, true];
    var len = baseFlags.length;


    var runFlags = new Array(len);
    var rq = ReqQue({
        onComplete: function() {
            assert.deepEqual(runFlags, baseFlags, "done");
            QUnit.start();
        }
    });
    for (var idx = 0; idx < len; idx++) {
        runFlags[idx] = false;
        rq.push({
            run: function(idx, queHandler) {
                runFlags[idx] = true;
                queHandler.done();
            }.bind(this, idx)
        });
    }
});

/**
 * リクエストが複数(同時実行数を越える)で、runメソッドが非同期になってない場合
 */
QUnit.asyncTest("multi_over_lim_req_serial", 1, function() {
    var baseFlags = [true, true, true, true, true, true, true, true, true];
    var len = baseFlags.length;


    var runFlags = new Array(len);
    var rq = ReqQue({
        onComplete: function() {
            assert.deepEqual(runFlags, baseFlags, "done");
            QUnit.start();
        }
    });
    for (var idx = 0; idx < len; idx++) {
        runFlags[idx] = false;
        rq.push({
            run: function(idx, queHandler) {
                runFlags[idx] = true;
                queHandler.done();
            }.bind(this, idx)
        });
    }
});


/**
 * リクエストが複数(同時実行数を越える)で、runメソッドが非同期になってる場合
 */
QUnit.asyncTest("multi_over_lim_req_async", 2, function() {
    var baseFlags = [true, true, true, true, true];
    var len = baseFlags.length;

    var timeout = [500, 500, 500, 100, 100];

    var runFlags = new Array(len);

    var rq = ReqQue({
        onComplete: function() {
            assert.deepEqual(runFlags, baseFlags, "done");
            QUnit.start();
        }
    });
    for (var idx = 0; idx < len; idx++) {
        runFlags[idx] = false;
        rq.push({
            run: function(idx, queHandler) {
                if (idx == 3) {
                    //3番目が開始する時点で0～2が終了していないとおかしい
                    assert.deepEqual(runFlags.slice(0, 3), [true, true, true], "run third req");
                }

                setTimeout(function() {
                    runFlags[idx] = true;
                    queHandler.done();
                }, timeout[idx]);
            }.bind(this, idx)

        });
    }
});

/**
 * 同時実行数を指定する、runメソッドが非同期になってる場合
 */
QUnit.asyncTest("multi_over_lim_req_async", 2, function() {
    var baseFlags = [true, true, true, true, true];
    var len = baseFlags.length;

    var timeout = [500, 500, 500, 100, 100];

    var runFlags = new Array(len);

    var rq = ReqQue({
        maxRun:4,
        onComplete: function() {
            assert.deepEqual(runFlags, baseFlags, "done");
            QUnit.start();
        }
    });
    for (var idx = 0; idx < len; idx++) {
        runFlags[idx] = false;
        rq.push({
            run: function(idx, queHandler) {
                setTimeout(function() {
                    if (idx == 3) {
                        //3番目が終了した時点で0～2が終了していたらおかしい
                        assert.deepEqual(runFlags.slice(0, 3), [false, false, false], "done third req");
                    }
                    runFlags[idx] = true;
                    queHandler.done();
                }, timeout[idx]);
            }.bind(this, idx)

        });
    }
});

QUnit.start();